/*
 * @Author: 吴灏
 * @Date: 2020-07-06 16:54:23
 * @LastEditors: 吴灏
 * @LastEditTime: 2020-07-30 17:19:54
 * @Description: file content
 */

import React, { useState, useEffect, useRef, useMemo } from 'react';
import { Input, message, Select, Modal, Tabs, DatePicker } from 'antd';
import globalStyles from '@/styles/global/index.less';
import { history } from 'umi';
import {
  organizationManageDetailQuery,
  organizationManageDetailSave,
  organizationManageDetailUpdate,
} from '@/pages/record/services';
import {
  ContactListItem,
  commonQueryDataFormat,
  HandleSearchType,
  TagValue,
} from '@/assets/typing/typings';
import { ColumnType } from 'antd/lib/table';
import { onValidate } from '@/utils/tools';
import Section from '@/components/Section';
import { columnsWidth } from '@/enums';
import { useLoading } from '@/layouts/BasicLayout';
import Table, { Store } from '@/components/FormTable';
import Layout from '@/components/Layout';
import Actions from '@/components/Actions';
import Form from '@/components/Form';
import { queryRecordFundAccounts } from '@/pages/record/fundAccounts/service';
import { queryContact } from '@/pages/record/contactDepartments/service';
import {
  settlementSearchColumns,
  accountSearchColumns,
} from '@/assets/publicConfig';
import ITable, { ITableRef } from '@/components/ITable';
import {
  queryReceivablesDetail,
  updateOrAddReceivables,
  auditReceivablesDetail,
  generateReceipt,
} from '@/pages/accounting/services';
import moment, { Moment } from 'moment';
import { IUpdateOrAddReceivables } from '@/pages/accounting/servicesTypings';

interface IProps {
  match: {
    params: { id: string };
  };
}

interface IFundColumns {
  id: string;
  name: string;
  enable: boolean;
  descirption: string;
}

interface IContactColumns {
  id: string;
  name: string;
  out_app_type: string;
  enable: boolean;
  descirption: string;
}

const OrganizationManageDetail: React.FC<IProps> = (props) => {
  const [form] = Form.useForm();

  const columns: ColumnType<IFundColumns>[] = [
    {
      title: '资金账户编号',
      dataIndex: 'id',
      key: 'id',
      width: columnsWidth.number,
    },
    {
      title: '资金账户名称',
      dataIndex: 'name',
      width: columnsWidth.name,
      key: 'name',
    },
    {
      title: '启用状态',
      dataIndex: 'enable',
      key: 'enable',
      width: columnsWidth.state,
      render: (text, record) => {
        return record.enable ? '启用' : '禁用';
      },
    },
    {
      title: '描述',
      dataIndex: 'description',
      key: 'description',
      width: columnsWidth.large,
    },
  ];

  const setLoading = useLoading()[1];

  const [dataSource, setDataSource] = useState<IFundColumns[]>([]);

  const updateFlag = useRef<boolean>(false);

  const popRef = useRef<ITableRef>(null);

  const store = useRef<Store<IFundColumns>>(null);

  const [selectedRows, setSelectedRows] = useState<React.ReactText[]>([]);

  const redirectIdRef = useRef<string>('');

  const auditSta = useRef<boolean>(false);

  const handleSearch = async (id: string) => {
    setLoading(true);
    const res = await queryReceivablesDetail({ id });
    setLoading(false);
    try {
      if (res && res.code === 0) {
        const { result } = res;
        form.setFieldsValue({
          id: result.id,
          business_type: result.business_type,
          state: result.state,
          date: result.date,
          deadline: result.deadline,
          total_amount: result.total_amount,
          name: result.settlement_unit.name,
        });
        return result;
      }
    } catch (error) {
      // console.log(error);
    }

    return [];
  };
  const init = async (id: string) => {
    handleSearch(id).then((result) => {
      // 根据单据审核状态决定按钮的disable
      if (result.state === 'CREATED') auditSta.current = false;
      else auditSta.current = true;
    });
  };
  // 保存
  const handleSave = async () => {
    const params: IUpdateOrAddReceivables = {
      date: form.getFieldValue('date'),
      deadline: form.getFieldValue('deadline'),
      id: form.getFieldValue('id'),
      details: [{ detail_num: 1, discount_rate: 2 }],
    };
    setLoading(true);
    const res = await updateOrAddReceivables(params);
    setLoading(false);
    if (res && res.code === 0) {
      message.success('更新成功');
      history.push('./');
    } else if (res.msg) message.error(res.msg);
    else message.error('更新失败');
  };
  // 审核单据
  const handleAudit = async () => {
    if (form.getFieldValue('state') === 'AUTITED')
      message.error('选中单据已审核');
    else {
      const params: IUpdateOrAddReceivables = {
        date: form.getFieldValue('date'),
        deadline: form.getFieldValue('deadline'),
        id: form.getFieldValue('id'),
        details: [{ detail_num: 1, discount_rate: 2 }],
      };
      setLoading(true);
      const res = await auditReceivablesDetail(params);
      setLoading(false);
      if (res && res.code === 0) {
        message.success('审核成功');
        history.push('./');
      } else if (res.msg) message.error(res.msg);
      else message.error('审核失败');
    }
  };
  // 生成收款单
  const handleGenerateReceipt = async () => {
    if (form.getFieldValue('state') === 'CREATED')
      message.error('未审核单据无法生成收款单');
    else if (form.getFieldValue('total_amount') > 0) {
      setLoading(true);
      const res = await generateReceipt([form.getFieldValue('id')]);
      setLoading(false);
      if (res && res.code === 0) {
        message.success('生成收款单成功');
      } else if (res.msg) message.error(res.msg);
      else message.error('生成收款单失败');
    } else {
      message.error('应收金额小于0，无法生成收款单');
    }
  };
  // 首次渲染
  useEffect(() => {
    const {
      match: {
        params: { id },
      },
    } = props;
    if (id) {
      init(id);
    }
  }, []);
  // 订阅路由的变化，随时变更跳转id
  useMemo(() => {
    if (
      props.match.params.id &&
      props.match.params.id !== ':id' &&
      redirectIdRef.current !== props.match.params.id
    ) {
      redirectIdRef.current = props.match.params.id;
      init(redirectIdRef.current);
    }
  }, [props.match.params.id]);

  return (
    <Layout style={{ backgroundColor: '#F4F4F4' }}>
      <Actions
        actions={[
          {
            text: '保存',
            action: handleSave,
          },
          {
            text: '审核',
            action: handleAudit,
          },
          {
            text: '关联业务',
            dropDown: [
              {
                text: '生成收款单',
                onClick: () => {
                  handleGenerateReceipt();
                },
              },
            ],
          },
          {
            text: '业务查询',
            dropDown: [
              {
                text: '核销记录',
                onClick: () => {
                  if (form.getFieldValue('state') === 'CREATED') {
                    message.error('未审核单据无法查询核销记录');
                  } else {
                    history.push(
                      `/accounting/receivableReceipt/verificationRecord/${form.getFieldValue(
                        'id'
                      )}`
                    );
                  }
                },
              },
            ],
          },
          {
            text: '返回',
            action: () => history.push('./'),
          },
        ]}
      />
      <Section title='基本'>
        <Form form={form}>
          <Form.Item label='单据编号' name='id'>
            <Input disabled />
          </Form.Item>
          <Form.Item label='单据类型' name='business_type'>
            <Input disabled />
          </Form.Item>
          <Form.Item label='状态' name='state'>
            <Input disabled />
          </Form.Item>
          <Form.Br />
          <Form.Item label='业务日期' name='date'>
            <DatePicker disabled={auditSta.current} />
          </Form.Item>
          <Form.Item label='到期日期' name='deadline'>
            <DatePicker disabled={auditSta.current} />
          </Form.Item>
          <Form.Item label='应收金额' name='total_amount'>
            <Input disabled />
          </Form.Item>
          <Form.Br />
          <Form.Item label='往来单位' name='name'>
            <Input disabled />
          </Form.Item>
        </Form>
        {/* <Table
          rowKey="id"
          storeRef={store}
          dataSource={data}
          onValuesChange={changedValues => {
            if (!('out_app_type' in changedValues)) {
              return;
            }

            return {
              description: changedValues['out_app_type'],
            };
          }}
          rowSelection={{
            selectedRowKeys: selectedRows,
            onChange: selectedRows => setSelectedRows(selectedRows),
          }}
          columns={columns}
        /> */}
      </Section>
    </Layout>
  );
};

export default OrganizationManageDetail;
