import React, { useState, useEffect, useRef } from 'react';
import { Button, Space, Tabs, message } from 'antd';
import PageLoading from '@/components/PageLoading';
import globalStyles from '@/styles/global/index.less';
// import styles from './index.less';
import { ColumnType } from 'antd/lib/table/interface';
import {
  queryDynamicDropdownInfo,
  queryDynamicTableInfo,
  queryCandidates,
  generateReceivable,
} from '@/pages/accounting/services';
import moment from 'moment';
import { Operators, orderName } from '@/enums/index';
import { PageInfo, TagValue } from '@/assets/typing/typings';
import { amountFormat } from '@/utils/tools';
import Section from '@/components/Section';
import Layout from '@/components/Layout';
import ProTableMini from './components/pro-table-mini';

const time = {
  today: moment(new Date(new Date().getTime())).format('YYYY-MM-DD'),
  tomorrow: moment(
    new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000)
  ).format('YYYY-MM-DD'),
};

function FastGenerate() {
  const exportRef = useRef(null);

  const [loading, setLoading] = useState<boolean>(false);
  const { TabPane } = Tabs;
  // 筛选条件配置
  const [summaryOfConditions, setSummaryOfConditions] = useState<any>({
    卡存款单: [],
    批发销售单: [],
    批发退货单: [],
    调出单: [],
    销售单据: [],
  });
  // 筛选值配置
  const [cardValues, setCardValues] = useState<any[]>([]);
  const [exportValues, setExportValues] = useState<any[]>([]);
  const [returnValues, setReturnValues] = useState<any[]>([]);
  const [saleValues, setSaleValues] = useState<any[]>([]);
  const [soldNoteValues, setSoldNoteValues] = useState<any[]>([]);

  // columns项
  const [cardColumns, setCardColumns] = useState<ColumnType<any>[]>([]);
  const [exportColumns, setExportColumns] = useState<ColumnType<any>[]>([]);
  const [returnColumns, setReturnColumns] = useState<ColumnType<any>[]>([]);
  const [saleColumns, setSaleColumns] = useState<ColumnType<any>[]>([]);
  const [soldNoteColumns, setSoldNoteColumns] = useState<ColumnType<any>[]>([]);
  // datasource的URL
  const [cardDSUrl, setCardDSUrl] = useState<string>('');
  const [exportDSUrl, setExportDSUrl] = useState<string>('');
  const [returnDSUrl, setReturnDSUrl] = useState<string>('');
  const [saleDSUrl, setSaleDSUrl] = useState<string>('');
  const [soldNoteDSUrl, setSoldNoteDSUrl] = useState<string>('');

  // TODO:还缺少一层tab的动态，后面再补充 2020/5/20 16:56
  // datasource数据
  const [cardDataSource, setCardDataSource] = useState([]);
  const [exportDataSource, setExportDataSource] = useState([]);
  const [returnDataSource, setReturnDataSource] = useState([]);
  const [saleDataSource, setSaleDataSource] = useState([]);
  const [soldNoteDataSource, setSoldNoteDataSource] = useState([]);

  const [cardTotalElements, setCardTotalElements] = useState<number>(0);
  const [exportTotalElements, setExportTotalElements] = useState<number>(0);
  const [returnTotalElements, setReturnTotalElements] = useState<number>(0);
  const [saleTotalElements, setSaleTotalElements] = useState<number>(0);
  const [soldNoteTotalElements, setSoldNoteTotalElements] = useState<number>(0);
  // 所选行
  const tabsRef = useRef<string>('批发销售单');
  const cardSelRows = useRef<string[]>([]);
  const exportSelRows = useRef<string[]>([]);
  const returnSelRows = useRef<string[]>([]);
  const saleSelRows = useRef<string[]>([]);
  const soldNoteSelRows = useRef<string[]>([]);

  // 开始渲染
  const startRender = useRef<boolean>(false);

  // 最终选择的筛选条件
  const finalFilterConditions = useRef({
    卡存款单: [],
    批发销售单: [],
    批发退货单: [],
    调出单: [],
    销售单据: [],
  });

  // 动态设置filter的value值
  const getDefaultSelectedDate = (ordertype: string) => {
    const requiredItems = summaryOfConditions[ordertype].filter(
      (item: { required: any }) => item.required
    );
    // return [
    //   {
    //     field: test[0].field,
    //     operator: Operators.BETWEEN,
    //     value: [time.today, time.tomorrow],
    //   },
    // ];
    return requiredItems.map(
      (item: {
        type: string;
        field: any;
        operators: string | any[];
        enums: { value: any }[];
      }) => {
        if (item.type === 'date') {
          return {
            field: item.field,
            operator: Operators.BETWEEN,
            value: [time.today, time.today],
          };
        }
        return {
          field: item.field,
          operator:
            item.operators.length > 1 ? item.operators : item.operators[0],
          // TODO:右框初始值为空是这儿？2020/6/3 11:14
          // DONE:是的
          // value: item.enums[0].value,
        };
      }
    );
  };
  const setDynamicFilterValue = (value: string | any[], ordertype: string) => {
    // 如果筛选条件为空，那么判定为获取筛选条件后的首次渲染，那么设定初值，否则动态设置当前值就可以了
    if (!(value.length > 0)) {
      if (summaryOfConditions[ordertype].length > 0)
        return getDefaultSelectedDate(ordertype);
      return [];
    }
    return value;
  };
  // 选择行
  const cardSelectedRows = (rows: any) => {
    cardSelRows.current = rows;
  };
  const exportSelectedRows = (rows: any) => {
    exportSelRows.current = rows;
  };
  const returnSelectedRows = (rows: any) => {
    returnSelRows.current = rows;
  };
  const saleSelectedRows = (rows: any) => {
    saleSelRows.current = rows;
  };
  const soldNoteSelectedRows = (rows: any) => {
    soldNoteSelRows.current = rows;
  };
  // 判断查询条件是否缺失函数
  const judgeRequiredConditionMissing = (
    params: { query_dtos: any[] },
    orderType: string | number
  ) => {
    // 筛选出所有的必须条件
    const requiredItems = summaryOfConditions[orderType].filter(
      (item: { query_key: any; required: any; type: string }) => {
        return item.required && item.type !== 'date';
      }
    );
    // 筛选出未携带的必选条件和携带了但为null的异常筛选条件
    return requiredItems.filter((item: { field: any }) => {
      const tmp = params.query_dtos.filter((item1) => {
        return item.field === item1.query_key;
      });
      if (tmp.length > 0 && tmp[0].query_value === undefined) return true;
      if (tmp.length === 0) return true;
      return false;
    });
  };
  // 搜索函数
  const handleSearch = async (url: string, params: any, orderType: string) => {
    finalFilterConditions.current = {
      ...finalFilterConditions.current,
      [orderType]: params.query_dtos,
    };
    // 这里就要从子组件抓取参数了
    setLoading(true);
    const res = await queryDynamicTableInfo(url, params);
    setLoading(false);
    if (res && res.code === 0) {
      return {
        total: res.result.total_elements,
        content: res.result.content.map((item: any, index: any) => {
          return { ...item, key: index };
        }),
      };
    }
    return null;
  };
  // TODO:根据改写过的pro-table-mini重新编写search函数 2020/5/22 19:03
  // TODO:从子组件获取参数请求datasource数据 2020/5/20 17:58
  // TODO:2020/5/21 9:55
  const cardSearch = (tags: TagValue<string>[], pageInfo: PageInfo) => {
    if (tags.length > 0) {
      setCardValues(tags);
      let params: any = {
        query_dtos: tags.map((item: any) => {
          return {
            query_key: item.field,
            query_status: item.operator,
            query_value:
              item.value instanceof Array ? item.value.join(',') : item.value,
          };
        }),
        page: pageInfo.page,
        size: pageInfo.size,
      };
      params = JSON.parse(JSON.stringify(params).replace('IN', 'EQUAL'));

      const requiredMissItem = judgeRequiredConditionMissing(
        params,
        '卡存款单'
      );
      if (requiredMissItem.length > 0) {
        requiredMissItem.map((item: { label: any }) =>
          message.error(
            `缺失必要查询条件“${item.label}”，请完善查询条件再进行查询！`
          )
        );
      } else {
        handleSearch(cardDSUrl, params, '卡存款单').then((val) => {
          if (val) {
            setCardTotalElements(val.total);
            setCardDataSource(val.content);
          }
        });
      }
    }
  };
  const exportSearch = (tags: TagValue<string>[], pageInfo: PageInfo) => {
    if (tags.length > 0) {
      setExportValues(tags);
      const params: any = {
        query_dtos: tags.map((item: any) => {
          return {
            query_key: item.field,
            query_status: item.operator,
            query_value:
              item.value instanceof Array ? item.value.join(',') : item.value,
          };
        }),
        page: pageInfo.page,
        size: pageInfo.size,
      };

      const requiredMissItem = judgeRequiredConditionMissing(params, '调出单');
      if (requiredMissItem.length > 0) {
        requiredMissItem.map((item: { label: any }) =>
          message.error(
            `缺失必要查询条件“${item.label}”，请完善查询条件再进行查询！`
          )
        );
      } else {
        handleSearch(
          exportDSUrl,
          JSON.parse(JSON.stringify(params).replace('IN', 'EQUAL')),
          '调出单'
        ).then((val) => {
          if (val) {
            setExportTotalElements(val.total);
            setExportDataSource(val.content);
          }
        });
      }
    }
  };
  const returnSearch = (tags: TagValue<string>[], pageInfo: PageInfo) => {
    if (tags.length > 0) {
      setReturnValues(tags);
      const params: any = {
        query_dtos: tags.map((item: any) => {
          return {
            query_key: item.field,
            query_status: item.operator,
            query_value:
              item.value instanceof Array ? item.value.join(',') : item.value,
          };
        }),
        page: pageInfo.page,
        size: pageInfo.size,
      };

      const requiredMissItem = judgeRequiredConditionMissing(
        params,
        '批发退货'
      );
      if (requiredMissItem.length > 0) {
        requiredMissItem.map((item: { label: any }) =>
          message.error(
            `缺失必要查询条件“${item.label}”，请完善查询条件再进行查询！`
          )
        );
      } else {
        handleSearch(
          returnDSUrl,
          JSON.parse(JSON.stringify(params).replace('IN', 'EQUAL')),
          '批发退货单'
        ).then((val) => {
          if (val) {
            setReturnTotalElements(val.total);
            setReturnDataSource(val.content);
          }
        });
      }
    }
  };
  const saleSearch = (tags: TagValue<string>[], pageInfo: PageInfo) => {
    if (tags.length > 0) {
      setSaleValues(tags);
      const params: any = {
        query_dtos: tags.map((item: any) => {
          return {
            query_key: item.field,
            query_status: item.operator,
            query_value:
              item.value instanceof Array ? item.value.join(',') : item.value,
          };
        }),
        page: pageInfo.page,
        size: pageInfo.size,
      };
      // TODO:这里运用判断函数来做空值校验 2020/6/3 12:01
      // DONE:已完成 2020/6/3 14:02
      const requiredMissItem = judgeRequiredConditionMissing(
        params,
        '批发销售'
      );

      if (requiredMissItem.length > 0) {
        requiredMissItem.map((item: { label: any }) =>
          message.error(
            `缺失必要查询条件“${item.label}”，请完善查询条件再进行查询！`
          )
        );
      } else {
        handleSearch(
          saleDSUrl,
          JSON.parse(JSON.stringify(params).replace('IN', 'EQUAL')),
          '批发销售单'
        ).then((val) => {
          if (val) {
            setSaleTotalElements(val.total);
            setSaleDataSource(val.content);
          }
        });
      }
    }
  };
  const soldNoteSearch = (tags: TagValue<string>[], pageInfo: PageInfo) => {
    if (tags.length > 0) {
      setSoldNoteValues(tags);
      const params: any = {
        query_dtos: tags.map((item: any) => {
          return {
            query_key: item.field,
            query_status: item.operator,
            query_value:
              item.value instanceof Array ? item.value.join(',') : item.value,
          };
        }),
        page: pageInfo.page,
        size: pageInfo.size,
      };
      // TODO:这里运用判断函数来做空值校验 2020/6/3 12:01
      // DONE:已完成 2020/6/3 14:02
      const requiredMissItem = judgeRequiredConditionMissing(
        params,
        '销售单据'
      );

      if (requiredMissItem.length > 0) {
        requiredMissItem.map((item: { label: any }) =>
          message.error(
            `缺失必要查询条件“${item.label}”，请完善查询条件再进行查询！`
          )
        );
      } else {
        handleSearch(
          soldNoteDSUrl,
          JSON.parse(JSON.stringify(params).replace('IN', 'EQUAL')),
          '销售单据'
        ).then((val) => {
          if (val) {
            setSoldNoteTotalElements(val.total);
            setSoldNoteDataSource(val.content);
          }
        });
      }
    }
  };

  // 生成应收单
  const generateReceivableOrder = async () => {
    const tabName = orderName[tabsRef.current];
    setLoading(true);
    const res = await generateReceivable({
      order_type: tabName,
      query_dtos: finalFilterConditions.current[tabsRef.current],
    });
    setLoading(false);
    if (res && res.code === 0) {
      message.success('生成应收单成功！');
      switch (tabName) {
        case '调出单':
          exportRef.current.fetch();
          break;
        default:
          break;
      }
    } else if (res.msg) message.error(res.msg);
    else message.error('生成应收单失败！');
  };
  // colunms格式化
  const colsFormat = (result: any) => {
    const res = result.columns.map((item: any) => {
      if (item.column_type === 'MONEY')
        return {
          title: item.name,
          dataIndex: item.code,
          align: 'right',
          render: (text: number) => {
            return amountFormat(text * 100);
          },
        };
      return { title: item.name, dataIndex: item.code };
    });
    return res;
  };
  // 初始化获取筛选条件
  const init = async () => {
    setLoading(true);
    const res = await queryDynamicDropdownInfo();
    setLoading(false);
    if (res && res.result) {
      /* 先获取查询条件 */
      // TODO:目前使用async 但是太慢了后面可以试试promise说不定可以解决问题 2020/5/27 14:48
      // DONE:已解决 2020/5/29 13:39
      const final = {
        卡存款单: [],
        批发销售: [],
        批发退货: [],
        调出单: [],
        销售单据: [],
      };
      const promiseRes = res.result.map(
        async (orderType: {
          conditions: {
            type: string;
            code: any;
            name: any;
            selector: any;
            candidate_path: string;
            required: any;
            ops: any;
          }[];
          name: string | number;
        }) => {
          let required = true;
          // TODO:就是这里，因为外层的map没有暂停住返回了promise  导致promise.all提前跳出了最外层await限制，周末必须解决了！ 2020/5/29 17:54
          // DONE:通过给内层所有map返回的promise套上一层promise.all从而解决问题 2020/5/29 20:59
          await Promise.all(
            orderType.conditions.map(
              async (searchConditions: {
                type: string;
                code: any;
                name: any;
                selector: any;
                candidate_path: string;
                required: any;
                ops: any;
              }) => {
                if (searchConditions.type === 'Date') {
                  if (required) {
                    required = false;
                    final[orderType.name].push({
                      field: searchConditions.code,
                      label: searchConditions.name,
                      type: 'date',
                      required: true,
                    });
                  } else {
                    final[orderType.name].push({
                      field: searchConditions.code,
                      label: searchConditions.name,
                      type: 'date',
                    });
                  }
                } else {
                  switch (searchConditions.selector) {
                    case 'COMBOBOX':
                      {
                        setLoading(true);
                        const cRes = await queryCandidates(
                          searchConditions.candidate_path
                        );
                        setLoading(false);
                        final[orderType.name].push({
                          required: searchConditions.required,
                          field: searchConditions.code,
                          label: searchConditions.name,
                          operators: searchConditions.ops,
                          type: 'select',
                          enums: cRes.result
                            ? cRes.result.map(
                                (ite: { value: any; literal: any }) => {
                                  return {
                                    label: ite.value,
                                    value: ite.literal,
                                  };
                                }
                              )
                            : [],
                        });
                      }
                      break;
                    case 'POPUP':
                    case 'NORMAL':
                      final[orderType.name].push({
                        required: searchConditions.required,
                        field: searchConditions.code,
                        label: searchConditions.name,
                        operators: searchConditions.ops,
                        type: 'input',
                      });
                      break;
                    default:
                      break;
                  }
                }
              }
            )
          );
        }
      );
      await Promise.all(promiseRes);
      // 启动渲染
      startRender.current = true;
      setSummaryOfConditions(final);
      /* 再获取columns的数据和datasource的URL */
      for (let i = 0; i < res.result.length; i += 1) {
        switch (res.result[i].name) {
          case '卡存款单':
            setCardColumns(colsFormat(res.result[i]));
            setCardDSUrl(res.result[i].query_path);
            break;
          case '批发销售':
            setSaleColumns(colsFormat(res.result[i]));
            setSaleDSUrl(res.result[i].query_path);
            break;
          case '批发退货':
            setReturnColumns(colsFormat(res.result[i]));
            setReturnDSUrl(res.result[i].query_path);
            break;
          case '调出单':
            setExportColumns(colsFormat(res.result[i]));
            setExportDSUrl(res.result[i].query_path);
            break;
          case '销售单据':
            setSoldNoteColumns(colsFormat(res.result[i]));
            setSoldNoteDSUrl(res.result[i].query_path);
            break;
          default:
            break;
        }
      }
    }
  };
  useEffect(() => {
    init();
  }, []);

  return (
    <div>
      {loading ? <PageLoading /> : null}
      {startRender.current ? (
        <Layout>
          <div className={globalStyles['title-buttons-wrapper']}>
            <Space>
              <Button onClick={generateReceivableOrder}>生成应收单</Button>
              {/* <Button>返回</Button> */}
            </Space>
          </div>
          {/* <div
            className="itable-filter-title"
            style={{
              marginLeft: '10px',
              marginRight: '10px',
              backgroundColor: 'white',
            }}
          >
            <div className="itable-filter-title-item">默认</div>
          </div> */}
          <Section title='默认' />
          <Tabs
            animated={false}
            className={globalStyles['fast-generate-tab']}
            onChange={(currentTabName) => {
              tabsRef.current = currentTabName;
            }}>
            {summaryOfConditions['批发销售'] ? (
              <TabPane tab='批发销售单' key='批发销售单'>
                <ProTableMini
                  // 查询条件
                  options={
                    summaryOfConditions['批发销售']
                      ? summaryOfConditions['批发销售']
                      : []
                  }
                  value={setDynamicFilterValue(saleValues, '批发销售')}
                  totalElements={saleTotalElements}
                  onSelected={saleSelectedRows}
                  // 查询函数
                  onSubmit={saleSearch}
                  // 列
                  columns={saleColumns}
                  // table的数据源
                  dataSource={saleDataSource}
                />
              </TabPane>
            ) : null}
            {summaryOfConditions['批发退货'] ? (
              <TabPane tab='批发退货单' key='批发退货单'>
                <ProTableMini
                  // 查询条件
                  options={
                    summaryOfConditions['批发退货']
                      ? summaryOfConditions['批发退货']
                      : []
                  }
                  value={setDynamicFilterValue(returnValues, '批发退货')}
                  totalElements={returnTotalElements}
                  onSelected={returnSelectedRows}
                  // 查询函数
                  onSubmit={returnSearch}
                  // 列
                  columns={returnColumns}
                  // table的数据源
                  dataSource={returnDataSource}
                />
              </TabPane>
            ) : null}
            {summaryOfConditions['卡存款单'] ? (
              <TabPane tab='卡存款单' key='卡存款单'>
                <ProTableMini
                  // 查询条件
                  options={
                    summaryOfConditions['卡存款单']
                      ? summaryOfConditions['卡存款单']
                      : []
                  }
                  value={setDynamicFilterValue(cardValues, '卡存款单')}
                  totalElements={cardTotalElements}
                  onSelected={cardSelectedRows}
                  // 查询函数
                  onSubmit={cardSearch}
                  // 列
                  columns={cardColumns}
                  // table的数据源
                  dataSource={cardDataSource}
                />
              </TabPane>
            ) : null}
            {summaryOfConditions['调出单'] ? (
              <TabPane tab='调出单' key='调出单'>
                <ProTableMini
                  actionRef={exportRef}
                  // 查询条件
                  options={
                    summaryOfConditions['调出单']
                      ? summaryOfConditions['调出单']
                      : []
                  }
                  value={setDynamicFilterValue(exportValues, '调出单')}
                  totalElements={exportTotalElements}
                  onSelected={exportSelectedRows}
                  // 查询函数
                  onSubmit={exportSearch}
                  // 列
                  columns={exportColumns}
                  // table的数据源
                  dataSource={exportDataSource}
                />
              </TabPane>
            ) : null}
            {summaryOfConditions['销售单据'] ? (
              <TabPane tab='销售单据' key='销售单据'>
                <ProTableMini
                  // 查询条件
                  options={
                    summaryOfConditions['销售单据']
                      ? summaryOfConditions['销售单据']
                      : []
                  }
                  value={setDynamicFilterValue(soldNoteValues, '销售单据')}
                  totalElements={soldNoteTotalElements}
                  onSelected={soldNoteSelectedRows}
                  // 查询函数
                  onSubmit={soldNoteSearch}
                  // 列
                  columns={soldNoteColumns}
                  // table的数据源
                  dataSource={soldNoteDataSource}
                />
              </TabPane>
            ) : null}
          </Tabs>
        </Layout>
      ) : null}
    </div>
  );
}

export default FastGenerate;
